# -*- makefile-bsdmake -*-
#
# This is a BSD Makefile, it will likely not work with GNU Make (but
# feel free to try). If you try to use this repo on Linux or other OS
# (good luck!), install bmake.

BASENAME=	blinky

# Set RUST_XTENSA & CROSSTOOLS_NG paths in Makefile.local

.include "xtensa-rust.mk"
