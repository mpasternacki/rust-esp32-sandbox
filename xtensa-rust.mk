# -*- makefile-bsdmake -*-
#
# This is a BSD Makefile include. It will likely not work with GNU
# Make (but feel free to try). If you try to use this repo on Linux or
# other OS, install bmake (and let me know how it went, good luck!).
#
# It's in a separate file to easily use it in multiple
# projects. Eventually it may even get its own repo or something.
#
# FreeBSD's fetch(1) is used to download files. I haven't found any
# portable port of it and it's a pity because it's more convenient to
# download files than curl or wget. I'll keep using fetch, but patches
# to support other tools welcome - as long as it keeps using fetch(1)
# by default.
#
# - `make` or `make all` will build the project
# - `make info` will show info about binary image
# - `make flash` will build the project if needed and flash it
# - `make console` will connect to serial console with cu(1) to show
#   logs or communicate with your firmware. NOTE: on disconnect it
#   seems to hang my esp32-thing, probably due to RTS/DTR reset
#   pin thing.
#
# You can set following variables on command line:
# - PROFILE=release or PROFILE=debug for Rust build profile
# - VERBOSE=1 for verbosity

# Local settings, next to makefile, should be .gitignored
.sinclude "Makefile.local"

## Variables to customize on command line, in Makefile.local, or in Makefile
##############################################################################

# Set to release for release build
PROFILE?=	debug

# Target binary name
# TODO: detect from Cargo.toml?
BASENAME?=	firmware

# Chip name
# TODO: detect from .cargo/config?
CHIP?=	esp32

# Target triples
CROSSTOOLS_NG_TARGET?=	xtensa-${CHIP}-elf
RUST_TARGET?=	xtensa-${CHIP}-none-elf

# baud rate for terminal
BAUDRATE?=	115200

# Use bootloader (needed for using irom, drom and psram)
# Valid values are:
# - arjanmels/esp32_bootloader_init_extram (default) - use https://github.com/arjanmels/esp32_bootloader_init_extram that initializes the external pram
# - arduino-esp32 - use default bootloader from the espressif arduino github
# - none - no bootloader
# - URL to bootloader binary (can be file:///, but path won't be treated as dependency)
#
# If you use arjanmels/esp32_bootloader_init_extram or arduino-esp32
# bootloader, you can set BOOTLOADER_REVISION to a specific
# branch/tag/commit on github (use :=). The default is set below,
# different for each of the two.
#
# TODO: distinguish custom bootloader path from URL?
BOOTLOADER?=	arjanmels/esp32_bootloader_init_extram

# address of partition table
PARTITION_ADDR?=	0x8000

# address of application
APP_ADDR?=	0x10000

# More flash variables
FLASH_MODE?=	dio
FLASH_SPEED?=	40m
FLASH_BEFORE?=	default_reset
FLASH_AFTER?=	hard_reset

# Partition table file
PARTITIONS?=	partitions.csv

# source of the utility to generate the binary partition table
GENPART_URL?=	https://github.com/espressif/esp-idf/blob/v4.0/components/partition_table/gen_esp32part.py?raw=true

## Customizable options (should) end here
###########################################

# Toolchain checks.
# TODO: if variable are defined, test for existence of paths
# TODO: check esptool.py & its version
.ifndef RUST_XTENSA
.error "Define RUST_XTENSA to path of a built checkout of https://github.com/MabezDev/rust-xtensa"
.elifndef CROSSTOOLS_NG
.error "Define CROSSTOOLS_NG to path of a built checkout of https://github.com/espressif/crosstool-NG"
.elifndef SERIAL
.error "Set SERIAL to path to your chip's serial port."
# TODO: this should be required only when serial is actually accessed
.endif

.if ${BOOTLOADER} == none
FLASH_TARGET:=	flash.no-bootloader
.else
# Find out bootloader URL
.if  ${BOOTLOADER} == arjanmels/esp32_bootloader_init_extram
BOOTLOADER_REVISION?=	v1.0
BOOTLOADER_URL:=	https://github.com/arjanmels/esp32_bootloader_init_extram/blob/${BOOTLOADER_REVISION}/build/bootloader/bootloader.bin?raw=true
.elif ${BOOTLOADER} == arduino-esp32
BOOTLOADER_REVISION?=	idf-release/v4.0
BOOTLOADER_URL:=	https://github.com/espressif/arduino-esp32/blob/${BOOTLOADER_REVISION}/tools/sdk/bin/bootloader_${FLASH_MODE}_${FLASH_SPEED}.bin?raw=true
.else
BOOTLOADER_URL:=	${BOOTLOADER}
.endif

FLASH_TARGET:=	flash.yes-bootloader
.endif

GENPART:=	target/gen_esp32part-${GENPART_URL:hash}.py
PARTITIONS_BIN:=	target/partitions.bin
BOOTLOADER_BIN:=	target/bootloader-${BOOTLOADER_URL:hash}.bin

# Xargo config & path to crosstools-ng
XARGO_RUST_URL:=	${RUST_XTENSA}/src
PATH:=	${CROSSTOOLS_NG}/builds/${CROSSTOOLS_NG_TARGET}/bin:${PATH}
.export XARGO_RUST_URL PATH

CARGO_OPTS:=
.if ${PROFILE} == release
CARGO_OPTS+=	--release
.elif ${PROFILE} == debug
# Nothing to do, cargo's default
.else
CARGO_OPTS+=	--profile ${PROFILE}
.endif

.if ${VERBOSE:U0}
CARGO_OPTS+=	--verbose
.endif

# These seem to have been needed with xargo _or_ when using cargo xbuild from a separate host rust install.
# Also, it includes host triple, so it shouldn't be hardcoded.
# TODO: look at this
# RUSTC:=${RUST_XTENSA}/build/x86_64-unknown-freebsd/stage2/bin/rustc
# RUSTDOC:=${RUST_XTENSA}/build/x86_64-unknown-freebsd/stage2/bin/rustdoc
# .export RUSTC RUSTDOC

# TODO: support examples

# Target paths
ELF=	target/${RUST_TARGET}/${PROFILE}/${BASENAME}
BIN=	${ELF}.bin

# Tools
ESPTOOL=	esptool.py --chip ${CHIP} --port ${SERIAL} --baud ${BAUDRATE} --before ${FLASH_BEFORE} --after ${FLASH_AFTER}
FLASH=		${ESPTOOL} write_flash --flash_mode ${FLASH_MODE} --flash_freq ${FLASH_SPEED} --flash_size detect

## Actual build
#################

.PHONY: all bin
all: ${ELF}
bin: ${BIN}

.PHONY: flash
flash: ${FLASH_TARGET}

.PHONY: download
download: ${GENPART} ${BOOTLOADER}

.PHONY: ${ELF}
${ELF}:
	cargo xbuild ${CARGO_OPTS}

${BIN}: ${ELF}
	${ESPTOOL} elf2image \
		--flash_mode ${FLASH_MODE} \
		--flash_freq ${FLASH_SPEED} \
		-o ${BIN} ${ELF}

${PARTITIONS_BIN}: ${PARTITIONS} ${GENPART}
	${GENPART} ${PARTITIONS} $@

.PHONY: flash.yes-bootloader
flash.yes-bootloader: ${BIN} ${PARTITIONS_BIN} ${BOOTLOADER_BIN}
# TODO:verify and skip unnecessary writes: ${ESPTOOL} verify_flash 0x1000 ${BOOTLOADER_BIN} ${PARTITION_ADDR} ${PARTITIONS_BIN}
	${FLASH} 0x1000 ${BOOTLOADER_BIN} ${PARTITION_ADDR} ${PARTITIONS_BIN} ${APP_ADDR} ${BIN}

.PHONY: flash.no-bootloader
flash.no-bootloader: ${BIN}
	${FLASH} -z 0x1000 ${BIN}

.PHONY: info
info: ${BIN}
	${ESPTOOL} image_info ${BIN}

${GENPART}:
	mkdir -p $$(dirname $@)
	fetch -o $@ -n '${GENPART_URL}'
	chmod 755 $@

${BOOTLOADER_BIN}:
	mkdir -p $$(dirname $@)
	fetch -o $@ -n '${BOOTLOADER_URL}'

.PHONY: clean
clean:
	cargo clean

.PHONY: console
console:
	cu -s ${BAUDRATE} -l ${SERIAL}
